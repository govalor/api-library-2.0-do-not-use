﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class Token
    {
        public class UserSettings
        {
            public string LanguageCode { get; set; }
            public string CountryCode { get; set; }
        }

        public class RootObject
        {
            public string access_token { get; set; }
            public string nwId { get; set; }
            public string TenantNameSpace { get; set; }
            public string TenantDomain { get; set; }
            public string nwServer { get; set; }
            public string ClientPath { get; set; }
            public string ClientURL { get; set; }
            public string NotificationServer { get; set; }
            public string ReportingServer { get; set; }
            public int Expiration { get; set; }
            public int RefreshWithNoActivity { get; set; }
            public string Tenant { get; set; }
            public string DefaultTheme { get; set; }
            public string Lifecycle { get; set; }
            public string Zone { get; set; }
            public string APIZone { get; set; }
            public string ValidZones { get; set; }
            public long TokenCreated { get; set; }
            public string TenantStripe { get; set; }
            public string Build { get; set; }
            public string TenantTimeZone { get; set; }
            public string TenantLanguageCode { get; set; }
            public string TenantCountryCode { get; set; }
            public string OrgUnitNwId { get; set; }
            public string DirectoryNwId { get; set; }
            public UserSettings UserSettings { get; set; }
            public string refresh_token { get; set; }
        }
    }
}
