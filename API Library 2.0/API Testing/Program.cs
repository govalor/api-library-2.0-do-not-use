﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API_Library_2._0;

namespace API_Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            Attendance attendance = new Attendance();
            List<GetFacultyClassAttendanceTaken_Result> att = new List<GetFacultyClassAttendanceTaken_Result>();

            att = attendance.getStudentGradesLoop();
            
            for(int i = 0; i < att.Count(); i++)
            {
                Console.WriteLine("Faculty: " + att.ElementAt(i).FacultyName);
                Console.WriteLine("Class: " + att.ElementAt(i).CourseName);
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
