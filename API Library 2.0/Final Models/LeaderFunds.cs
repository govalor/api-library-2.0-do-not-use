﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    public class LeaderFunds
    {
        public string LeaderId { get; set; }
        //public string LeaderName { get; set; }
        public string FundId { get; set; }
        public string ExperienceDescription { get; set; }
    }
}
