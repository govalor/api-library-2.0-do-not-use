﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    public class SolicitorFunds
    {
        public string SolicitorId { get; set; }
        public string FundId { get; set; }
        public string Description { get; set; }
    }
}
