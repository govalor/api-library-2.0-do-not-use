﻿using System;
using System.Web;

namespace API_Library_2._0
{
    public class StudentGradesModel
    {
        public int EA7RecordsID { get; set; }

        public string StudentID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int GradeLevel { get; set; }

        public string SectionCode { get; set; }

        public string CourseSection { get; set; }

        public string Faculty { get; set; }

        public string Room { get; set; }

        public string Course { get; set; }

        public string ClassName { get; set; }

        public double Grade { get; set; }


    }
}