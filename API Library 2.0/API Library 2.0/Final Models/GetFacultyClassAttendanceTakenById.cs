﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{

    public partial class GetFacultyClassAttendanceTakenById
    {

        public int SectionCode { get; set; }

        public string BlockColor { get; set; }

        public string CourseName { get; set; }

        public string CourseCode { get; set; }

        public string SectionTitle { get; set; }

        public string FacultyName { get; set; }

        public Nullable<int> FacultyId { get; set; }

        public string FacultyEmail { get; set; }
    }

    public partial class GetFacultyClassAttendanceTaken_Result
    {
        public int SectionCode { get; set; }
        public string BlockColor { get; set; }
        public string CourseName { get; set; }
        public string SectionTitle { get; set; }
        public string FacultyName { get; set; }
        public Nullable<int> FacultyId { get; set; }
        public string FacultyEmail { get; set; }
        public string CourseCode { get; set; }
    }
}
